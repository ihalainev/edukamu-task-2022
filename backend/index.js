const express = require("express");
const app = express();
const port = 5000;
const endpoint = "https://europe-west1-dev-edukamu.cloudfunctions.net/users";
const cors = require("cors");

/*app.use(
  cors({
    origin: "https://localhost:3000",
  })
)

app.get('/products/:id', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})
/**
 * TODO: Endpoints
 */


/**
 * Setup
 */
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

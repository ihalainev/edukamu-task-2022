import React from "react";
import userData  from "./data/users.json";
import { useState, useEffect } from "react";
import axios from "axios";
const url = "https://europe-west1-dev-edukamu.cloudfunctions.net/users";



const HomePageHeader = () => {
    return (
      <header className="header">
        <h2>Your Address Book</h2>
      </header>
    );
  };

  const Info = ({ name, email, address }) => {
    return (
      <table>
        <tbody>
          <tr>
            <td>
              <h5>{name}</h5>
            </td>
            <td>
              <h5>{email}</h5>
            </td>
            <td>
              <h4>{address}</h4>
            </td>
          </tr>
        </tbody>
      </table>
    );
  };

const User = () => {

  /*
  Alla olevassa getUserData()-funktiossa käytetään "axios" kirjastoa, joka
  on promise-pohjainen kirjasto, jolla voidaan korvata perinteisesti
  käytetyn promise.then() funktion async/await toiminnolla. 
  Kirjaston get()-metodi ottaa argumenttina URL-osoitteen ja tekee
  http-pyynnön tälle URL-osoitteelle ja muuntaa sen vastauksen automaattisesti
  JSON-muotoon, joka voidaan saada sen data-ominaisuudesta.
  Kun tiedot on vastaanotettu, komponentin tila päivitetään setUserData()-funktion kautta.
  

  const [userData, setUserData] = useState({});

  useEffect(() => {
    getUserData();
  }, []);
 
  const getUserData = async () => {
    const response = await axios.get(url);
    setUserData(response.data);
  };*/

    return ( 
        <>
        <HomePageHeader />
        <div className="user-container">
          {userData.map((data, key) => {
            return (
              <div key={key}>
                <Info
                    key={key}
                    name={data.name}
                    email={data.email}
                    address={data.address}
                  />
              </div>
            );
          })}
        </div>
        </>

     );
}
 
export default User;